from django.contrib.auth.models import User

from rest_framework.generics import (
   RetrieveAPIView,
)
from .serializers import UserDetailSerializer



class UserDetailAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    lookup_field = 'id'
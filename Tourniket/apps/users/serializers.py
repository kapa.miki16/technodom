from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Profile



class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('image', 'id')

class UserDetailSerializer(serializers.ModelSerializer):
    profile = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email', 'profile')

    def get_profile(self, obj):
        if obj.profile == None:
            return ProfileSerializer(obj.profile, many=False).data
        return None